function showAlert(title, message, button) {
    navigator.notification.alert(
        message,        // message
        alertDismissed, // callback
        title,          // title
        button          // buttonName
    );
}

function loadView(page) {
	$.get('views/'+page+'.html',
		function(data) {
			$('#main-body').html(data);
            if(typeof initPage == 'function') {
                initPage();
            }
		}
	);
}
